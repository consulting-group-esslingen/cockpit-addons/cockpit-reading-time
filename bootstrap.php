<?php
/**
 *	Cockpit Reading Time
 *  bootstrap.php
 *	Created on 2021-01-24 13:31
 *
 *  @author Consulting Group Esslingen e.V.
 *  @copyright Copyright (c) 2021 cg-esslingen.de
 *  @license MIT
 *
 */

$this->module('readingtime')->extend([

  'config' => function() {

    static $config;

    if (!isset($config)) {

      $defaultConfig = [
        'field_name' => 'reading_time',
        'words_per_minute' => 200,
      ];

      $config = array_replace_recursive(
        $defaultConfig,
        $this->app->retrieve('reading_time', [])
      );

    }

    return $config;

  },

  'readingTime' => function($name, $entry) {

    $config = $this->config();

    if (!$config) return $entry;

    // get field name name
    $fieldName = $config['field_name'];

    // get field name
    if (isset($config['collections'][$name])) {

        $field = $config['collections'][$name];

        $contentLength = $this->getContentLength($entry, $field);
        $wordsPerMinute = $config['words_per_minute'];
        $wordsPerSecond = $wordsPerMinute / 60;

        $readingTime = [
          'words' => $contentLength,
          'seconds' => ceil($contentLength / $wordsPerSecond),
          'minutes' => ceil($contentLength / $wordsPerMinute),
        ];

        // save reading time to entry
        $entry[$fieldName] = $readingTime;

    }

    return $entry;

  },

  'getContentLength' => function($entry, $field) {

    $content = $entry[$field];
    $content = strip_tags($content);

    return str_word_count($content);

  },

]);

// set events
$this->on('cockpit.bootstrap', function() {

  $config = $this->module('readingtime')->config();

  if (!$config) return;

  if (isset($config['collections']) && is_array($config['collections'])) {

    foreach ($config['collections'] as $col => $field) {

      $this->on("collections.save.before.$col", function($name, &$entry, $isUpdate) {
        $entry = $this->module('readingtime')->readingTime($name, $entry);
      });

    }

  }

}, 100);