# Cockpit Reading Time

Add an estimated reading time to your Cockpit entries


## Usage

### Installation

Download the [latest release as a .zip-file](https://gitlab.com/consulting-group-esslingen/cockpit-addons/cockpit-reading-time/-/releases) and extract to `your-cockpit-root/addons` (e.g. `cockpit/addons/ReadingTime`).

> ℹ️ Currently there is no latest release but you can [download the current master branch](https://gitlab.com/consulting-group-esslingen/cockpit-addons/cockpit-reading-time/-/archive/master/cockpit-reading-time-master.zip)


### Configuration

***See below for an example configuration***

The following configuration options are available (in `config/config.yaml`):


##### Available Configuration Options

|*Key*|Default|Notes|
|---|---|---|
|`words_per_minute`|200|How many word per minute the average reader is able to process (pessimistic approach). Modify to fit your language.|
|`field_name`|`reading_time`|The name of the field which contains the reading time in the api response.|
|`collections`|-|**REQUIRED** A list of collections and the field name of which you want to calculate the reading time (`*collection_name*: *field*`).|

The only **required** option is `collections`, which specifies which collections to calculate the reading time for.


##### Example configuration

```yaml
reading_time:
    words_per_minute: 220
    collections:
        blog_posts: content 
```


### Frontend

Use the reading time response however you like. The field (you can set the field name, see [Configuration](#configuration)) will be a JSON object containing information about the reading time:

```json
"reading_time": {
  "words": 5012
  "minutes": 23,
  "seconds": 1367
}
```

* `reading_time.words` is the total word count for your content
* `reading_time.minutes` is the reading time in minutes (rounded up)
* `reading_time.words` is the reading time in seconds (rounded up) in case you want to display a more specific time


## TODO

- [ ] Multilanguage support
- [ ] Support for content in nested fields
- [ ] Find a way to convert markdown to plain text (HTML is already working)


## Thanks

* To [Agentejo](https://agentejo.com) for creating Cockpit!


## Copyright and License

Copyright 2021 Consulting Group Esslingen e.V. under the MIT license.
